package br.com.dbc.minhaSeguradora.service;

import br.com.dbc.minhaSeguradora.entity.Apolice;
import br.com.dbc.minhaSeguradora.repository.ApoliceRepository;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author claudia.moura
 */

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public abstract class AbstractCrudService<E> {
    
    protected abstract JpaRepository<E, Long> getRepository();
    
    @Transactional(readOnly = false)
    public E save(@NotNull @Valid E e) {
        return getRepository().save(e);
    }
    
    @Transactional(readOnly = false)
    public void delete(Long id) {
        getRepository().deleteById(id);
    }
        
    public Page<E> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }
    
    public Optional<E> findById(Long id) {
        return getRepository().findById(id);
    }
    
}
