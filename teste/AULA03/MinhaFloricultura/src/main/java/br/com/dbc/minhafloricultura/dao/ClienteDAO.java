package br.com.dbc.minhafloricultura.dao;

import br.com.dbc.minhafloricultura.entity.Cliente;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author claudia.moura
 */
@Stateless
public class ClienteDAO extends AbstractDAO<Cliente> {

    @PersistenceContext(unitName = "minha_floricultura_pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteDAO() {
        super(Cliente.class);
    }
    
}
