package br.com.dbc.petshopjpa.dao;

import br.com.dbc.petshopjpa.entity.Cliente;
import com.sun.istack.internal.logging.Logger;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.EntityManager;

/**
 *
 * @author claudia.moura
 */
public class ClienteDAO {

    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(ClienteDAO.class.getName());
    
    public List<Cliente> findAll() {
        EntityManager em = PersistenceUtils.getEntityManager();
        return em.createQuery("Select c from Cliente c").getResultList();
    }
    
    public Cliente findOne(Long id) {
        EntityManager em = PersistenceUtils.getEntityManager();
        return em.createQuery("Select c from Cliente c where c.id = :id", Cliente.class)
            .setParameter("id", id)
            .getSingleResult();
    }
    
    public void delete(Long id) {
        EntityManager em = PersistenceUtils.getEntityManager();
        em.createQuery("Delete from Cliente c where c.id = :id", Cliente.class)
            .setParameter("id", id)
            .executeUpdate();
    }
    
    public void create(Cliente cliente) {
        EntityManager em = PersistenceUtils.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(cliente);
            em.getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback();
        }
    }
    
    public void update(Cliente cliente) {
        EntityManager em = PersistenceUtils.getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(cliente);
            em.getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback();
        }
    }
}
