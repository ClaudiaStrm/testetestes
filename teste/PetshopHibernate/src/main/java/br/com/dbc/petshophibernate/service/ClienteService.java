package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.entity.Cliente;
/**
 *
 * @author claudia.moura
 */
public class ClienteService extends AbstractCRUDService<Cliente, Long, ClienteDAO> {

    private static ClienteService instance;

    static {
        instance = new ClienteService();
    }

    public static ClienteService getInstance() {
        return instance;
    }
    
    @Override
    protected ClienteDAO getDAO() {
        return ClienteDAO.getInstance();
    }
}
