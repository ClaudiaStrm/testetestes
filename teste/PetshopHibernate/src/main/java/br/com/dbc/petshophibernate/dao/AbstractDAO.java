package br.com.dbc.petshophibernate.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 * @author claudia.moura
 */
public abstract class AbstractDAO<ENTITY, ID> {

    private static final Logger LOG = Logger.getLogger(AbstractDAO.class.getName());

    public static Object getInstance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected abstract Class<ENTITY> getEntityClass();

    protected abstract String getIdProperty();
    
    public AbstractDAO() {
    }

    public ENTITY findById(ID id) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory()
                    .openSession();
            return (ENTITY) session
                    .createCriteria(getEntityClass())
                    .add(Restrictions.eq(getIdProperty(), id))
                    .uniqueResult();
        } catch (Exception exception) {
            LOG.log(Level.SEVERE, exception.getMessage(), exception);
            throw exception;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<ENTITY> findAll() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createCriteria(getEntityClass()).list();
        } catch (Exception exception) {
            LOG.log(Level.SEVERE, exception.getMessage(), exception);
            throw exception;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public void createOrUpdate(ENTITY e) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(e);
            transaction.commit();
        } catch (Exception exception) {
            LOG.log(Level.SEVERE, exception.getMessage(), exception);
            throw exception;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public void delete(ENTITY e) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(e);
            transaction.commit();
        } catch (Exception exception) {
            LOG.log(Level.SEVERE, exception.getMessage(), exception);
            throw exception;
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }
}

//    public ENTITY findById(Long id) {
//        return (ENTITY) HibernateUtil.getSessionFactory()
//                .openSession()
//                .createCriteria(entityClass)
//                .add(Restrictions.eq(idProperty, id))
//                .uniqueResult();
//    }
