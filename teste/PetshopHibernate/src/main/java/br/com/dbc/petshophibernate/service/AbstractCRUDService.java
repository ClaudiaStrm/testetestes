/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.AbstractDAO;
import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.entity.AbstractEntity;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.List;
import javassist.NotFoundException;

/**
 *
 * @author claudia.moura
 */
public abstract class AbstractCRUDService
            <ENTITY extends AbstractEntity<ID>, ID, DAO extends AbstractDAO<ENTITY, ID>> {
       
    protected abstract DAO getDAO();

    public List<ENTITY> findAll(){
        return getDAO().findAll();
    }

    public ENTITY findById(ID id) {
        return getDAO().findById(id);
    }

    public void create(ENTITY entity) {
        if (entity.getId() != null) {
            throw new IllegalArgumentException("Criação de cliente não pode ter ID");
        }
        getDAO().createOrUpdate(entity);
    }

    public void update(ENTITY entity) {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("Atualização de cliente precisa ter ID");
        }
        getDAO().createOrUpdate(entity);
    }

    public void delete(ID id) throws NotFoundException {
        if (id == null) {
            throw new IllegalArgumentException("ID deve ser informado");
        }
        ENTITY entity = getDAO().findById(id);
        if (entity == null) {
            throw new NotFoundException("");
        }
        getDAO().delete(entity);
    }    
}
